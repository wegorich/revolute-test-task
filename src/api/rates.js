import apiKey from './config/apiKey';

const RATES_REQUEST = `https://openexchangerates.org/api/latest.json?app_id=${apiKey}`;
const fetch = window.fetch;

function responseData(response) {
  //can be included custom parsing for response
  return response.text().then(JSON.parse);
}

function checkResponse(response) {
  if (response.ok) {
    return response;
  }
  throw new Error('Bad response');
}

function logError(e) {
  console.log(e);
}

// cause we cant change base currency using this api
const recalcCostDependsOnBase = (rates, base, newBase, target) => {
  const diffValue = rates[base] / rates[newBase];
  return newBase === target ? 1 : rates[target] * diffValue;
}

const prepareRatesTable = (currencies) => ({ base, rates }) => {
  const allRates = { ...rates, [base]: 1 };
  return currencies.reduce((result, current) => {
    result[current] = {};

    currencies.reduce((res, cur) => {
      res[cur] = recalcCostDependsOnBase(allRates, base, current, cur);

      return res;
    }, result[current])
    return result;
  }, {});
};

async function rates() {
  const response = await fetch(RATES_REQUEST);
  const response_1 = await checkResponse(response);
  return responseData(response_1);
}

export default (currencies) => rates().then(prepareRatesTable(currencies)).catch(logError);
