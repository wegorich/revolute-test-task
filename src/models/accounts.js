import { Container } from 'unstated';
import uuid from 'uuid/v4';
import { currencyCode } from './currency';

export class AccountsContainer extends Container {
  state = {
    accounts: [{
      currencyCode: currencyCode.EUR,
      amount: 100.75,
      id: uuid(),
    }, {
      currencyCode: currencyCode.GBP,
      amount: 100.75,
      id: uuid(),
    }, {
      currencyCode: currencyCode.USD,
      amount: 100.75,
      id: uuid(),
    }],
  }

  getAccount(accountId) {
    const { accounts } = this.state;
    return accounts.find(e => e.id === accountId);
  }

  accountBalance(accountId) {
    const account = this.getAccount(accountId);

    return account && account.amount;
  }

  isExchangeValid(exchange) {
    const { fromAccountId, amount } = exchange;
    return this.accountBalance(fromAccountId) >= amount;
  }


  transfer(fromAccountId, toAccountId, fromAmount, toAmount) {
    const fromAccount = this.getAccount(fromAccountId);
    const toAccount = this.getAccount(toAccountId);
    fromAccount.amount = fromAccount.amount - fromAmount;
    toAccount.amount = toAccount.amount + toAmount;

    this.setState({
      accounts: [].concat(this.state.accounts)
    })
  }

  exchange = (exchange) => {
    if (this.isExchangeValid(exchange)) {
      const { fromAccountId, toAccountId, amount, exchangeAmount } = exchange;

      this.transfer(fromAccountId, toAccountId, amount, exchangeAmount);
    }
  }
}
