export const currencyCode = {
  GBP: 'GBP',
  EUR: 'EUR',
  USD: 'USD',
};

export const currencyToSymbols = {
  [currencyCode.GBP]: '£',
  [currencyCode.EUR]: '€',
  [currencyCode.USD]: '$'
};
