import { Container } from 'unstated';
import loadRates from '../api/rates';
import { currencyCode } from './currency';

export class RatesContainer extends Container {
  state = {
    rates: null,
    filter: Object.keys(currencyCode)
  };

  constructor() {
    super();

    this.loadRates();
  }

  loadRates = async () => {
    const rates = await loadRates(this.state.filter);
    this.setState({ rates: rates });
  };

  startUpdates = () => {
    this.stopUpdates();
    this.__updateInterval = setInterval(this.loadRates, 10000);
  }

  stopUpdates = () => {
    clearInterval(this.__updateInterval);
  }
}
