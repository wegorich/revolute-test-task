import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { Dots } from '../Dots/Dots';

export class SwipeableContainer extends React.PureComponent {
  state = {
    currentIndex: this.props.defaultIndex || 0,
    isTransitioning: false,
  };

  onIndexChange = (index) => {
    this.setState({
      currentIndex: index
    }, () => this.props.onChange && this.props.onChange(this.state.currentIndex));
  };

  onSwitching = () => {
    if (this.state.isTransitioning) return;

    this.setState({
      isTransitioning: true,
    })
  }

  onTransitionEnd = () => {
    this.setState({
      isTransitioning: false,
    })
  }

  render() {
    const { className, children } = this.props;
    const { currentIndex, isTransitioning } = this.state;

    return (
      <div className={className}>
        <SwipeableViews
          index={currentIndex}
          className={isTransitioning ? 'transition_on' : 'transition_off'}
          onSwitching={this.onSwitching}
          onTransitionEnd={this.onTransitionEnd}
          onChangeIndex={this.onIndexChange}
        >
          {children}
        </SwipeableViews>
        <Dots count={children.length} index={currentIndex} />
      </div>
    );
  }
}
