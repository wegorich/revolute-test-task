import React from 'react';
import { Rate } from '../Rate/Rate';
import './ExchangeRate.css';

export const ExchangeRateDropdown = ({onClick, exchangeRates: { from, to, rates}}) => {
  return rates ? <div className='exchange-rate exchange-rate__dropdown ' onClick={onClick}>
      <Rate code={from} value={rates[from][from]} />
      <span className='exchange-rate__separator'>=</span>
      <Rate code={to} value={rates[from][to]} />
      <span className='exchange-rate__angle'>&#xfe40;</span>
    </div> : <div className='exchange-rate exchange-rate__dropdown '>
      <span className='exchange-rate__placeholder'></span>
      <span className='exchange-rate__angle'>&#xfe40;</span>
    </div>
};

export const ExchangeRate = ({ exchangeRates: { from, to, rates} }) => {
  return rates ? <div className='exchange-rate'>
      <Rate code={from} value={rates[from][from]} />
      <span className='exchange-rate__separator'>=</span>
      <Rate code={to} value={rates[from][to]} />
    </div> : <div className='exchange-rate'>
      <span className='exchange-rate__placeholder'></span>
    </div>;
};
