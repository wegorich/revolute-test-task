import React from 'react';
import numeral from 'numeraljs';
import { currencyToSymbols } from '../../models/currency';

import './Rate.css';

export const Rate = ({ code, value }) => {
  const fomattedValue = numeral(value).format('0.[00]');
  let lastAmount = numeral(value).format('.[0000]');
  lastAmount = lastAmount.length > 3 ? lastAmount.substr(3, lastAmount.length) : '';

  return (
    <span className='rate'>
      <span className='rate__small'>{currencyToSymbols[code]}</span>
      <span className='rate__nubmer'>{fomattedValue}</span>
      {lastAmount && <span className='rate__small'>{lastAmount}</span>}
    </span>
  );
};
