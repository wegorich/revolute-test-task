import React from 'react';
import './Dots.css';

export const Dots = React.memo(({count, index}) => {
  const generateDotsArr = (length) => new Array(length).fill(0);
  const arr = generateDotsArr(count);

  return (
    <div className="dot__wr">
      {arr.map((e, dotIndex) => {
        return <span
          key={dotIndex}
          className={`dot ${index === dotIndex ? 'dot--active' : ''}`} />
      })}
    </div>
  )
});
