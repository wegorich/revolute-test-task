import React from 'react';
import { Rate } from '../Rate/Rate';
import numeral from 'numeraljs';
import './ExchangeCurrency.css';

export class ExchangeCurrency extends React.PureComponent {
  render() {
    let { isDark, code, amount, value, onChange, rateSlot } = this.props;
    const val = value ? ((value + '').endsWith('.') ? value : numeral(value).format('0.[00]')) : '';

    return (
      <label className={`exchange__currency ${isDark ? 'exchange__currency--dark' : ''}`}>
        <div className='exchange__group'>
          <div className='exchange__group-main'>{code}</div>
          <div className='exchange__group-desc'>
            <span>You have</span>
            <span className='exchange__group-desc-space' />
            <Rate code={code} value={amount} />
          </div>
        </div>
        <div className='exchange__group'>
          <div className='exchange__group-main'>
            <input className='exchange__input' value={val} onInput={onChange} onChange={onChange} />
          </div>
          {rateSlot && <div className='exchange__group-desc'>{rateSlot}</div>}
        </div>
      </label>
    );
  }
}
