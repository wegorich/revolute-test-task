import React from 'react';
import 'devices.css';
import './DeviceFrame.css';

export const DeviceFrame = ({ children }) => {
  return (<div className='device-frame'>
      <div className='device device-iphone-x'>
        <div className='device-frame'>{children}</div>
        <div className='device-stripe' />
        <div className='device-header' />
        <div className='device-sensors' />
        <div className='device-btns' />
        <div className='device-power' />
      </div>
    </div>
  );
};
