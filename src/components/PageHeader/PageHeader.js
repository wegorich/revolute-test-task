import React from 'react'
import './PageHeader.css';

export const PageHeader = ({children, rightAction, leftAction}) => {
  return (
    <div className='page__header'>
      {leftAction}
      <div className='page__header-content'>
        {children}
      </div>
      {rightAction}
    </div>
  )
}
