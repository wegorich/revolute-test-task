import React from 'react';
import { NumericKeyboard } from 'numeric-keyboard/dist/numeric_keyboard.react';
import { Button } from '../components/Button/Button';
import { ExchangeRateDropdown, ExchangeRate } from '../components/ExchangeRate/ExchangeRate';
import { PageHeader } from '../components/PageHeader/PageHeader';

import './Exchange.css';
import { ExchangeCurrency } from '../components/ExchangeCurrency/ExchangeCurrency';
import { SwipeableContainer } from '../components/SwipeableContainer/SwipeableContainer';

export class Exchange extends React.PureComponent {
  state = {
    fromAccount: this.props.accounts[0],
    toAccount: this.props.accounts[1],
    fromValue: 0,
    toValue: 0
  };

  componentDidMount() {
    this.props.ratesStartUpdates && this.props.ratesStartUpdates();
  }

  componentWillUnmount() {
    this.props.ratesStopUpdates && this.props.ratesStopUpdates();
  }

  onFromChanged = (index) => {
    const { accounts } = this.props;

    this.setState(
      {
        fromAccount: accounts[index]
      },
      () => {
        this.onToValueChanged({ target: { value: this.state.toValue } });
      }
    );
  };

  onToChanged = (index) => {
    const { accounts } = this.props;

    this.setState(
      {
        toAccount: accounts[index]
      },
      () => {
        this.onFromValueChanged({ target: { value: this.state.fromValue } });
      }
    );
  };

  changeInputValues = (from, to) => {
    const { rates } = this.props;
    const { fromAccount, toAccount } = this.state;
    const isFromHigher =
      toAccount.amount >= fromAccount.amount / rates[fromAccount.currencyCode][toAccount.currencyCode];

    if (from < 0 || to < 0 || from > fromAccount.amount || to > toAccount.amount) {
      if (isFromHigher) {
        from = fromAccount.amount;
        to = from / rates[fromAccount.currencyCode][toAccount.currencyCode];
      } else {
        to = toAccount.amount;
        from = to / rates[toAccount.currencyCode][fromAccount.currencyCode];
      }
    }

    this.setState({
      toValue: to,
      fromValue: from
    });
  };

  onToValueChanged = (e) => {
    const { fromAccount, toAccount } = this.state;
    const { rates } = this.props;

    this.changeInputValues(
      e.target.value * 1 / rates[toAccount.currencyCode][fromAccount.currencyCode],
      e.target.value
    );
  };

  onFromValueChanged = (e) => {
    const { fromAccount, toAccount } = this.state;
    const { rates } = this.props;

    this.changeInputValues(e.target.value, e.target.value / rates[fromAccount.currencyCode][toAccount.currencyCode]);
  };

  onExchangeClick = () => {
    const { exchange } = this.props;
    const { toAccount, fromAccount, fromValue, toValue } = this.state;

    exchange &&
      exchange({
        fromAccountId: fromAccount.id,
        toAccountId: toAccount.id,
        amount: fromValue,
        exchangeAmount: toValue
      });

    this.changeInputValues(0, 0);
  };

  onInput = (e) => {
    if (e === 'enter') return;

    const { fromValue } = this.state;
    const strVal = fromValue + '';
    const val = e !== 'del' ? parseFloat(strVal + e) : parseFloat(strVal.substr(0, strVal.length - 1)) || 0;

    this.onFromValueChanged({ target: { value: val } });
  };

  onBack = () => {
    alert('Nice try! What do you expects?')
  }

  dropDown = () => {
    alert('Fancy dropdown awailable only in PRO subscription');
  }

  render() {
    const { rates, accounts } = this.props;
    const { toAccount, fromAccount, fromValue, toValue } = this.state;

    const canExchange = toAccount.currencyCode !== fromAccount.currencyCode && fromValue && toValue;

    return (
      <div className='exchange'>
        <div className='exchange__wr' />
        <div className='exchange__content'>
          <PageHeader
            leftAction={<Button onClick={this.onBack}>Cancel</Button>}
            rightAction={
              <Button disabled={!canExchange} onClick={this.onExchangeClick}>
                Exchange
              </Button>
            }
          >
            <ExchangeRateDropdown
              onClick={this.dropDown}
              exchangeRates={{ from: fromAccount.currencyCode, to: toAccount.currencyCode, rates }}
            />
          </PageHeader>
          <SwipeableContainer className='exchange__swipable' onChange={this.onFromChanged}>
            {accounts.map((account) => {
              return (
                <ExchangeCurrency
                  key={account.id}
                  value={fromValue}
                  onChange={this.onFromValueChanged}
                  code={account.currencyCode}
                  amount={account.amount}
                />
              );
            })}
          </SwipeableContainer>
          <div className='exchange__currency-angle' />
          <SwipeableContainer defaultIndex={1} className='exchange__swipable' onChange={this.onToChanged}>
            {accounts.map((account) => {
              return (
                <ExchangeCurrency
                  key={account.id}
                  isDark
                  code={account.currencyCode}
                  amount={account.amount}
                  value={toValue}
                  onChange={this.onToValueChanged}
                  rateSlot={
                    <ExchangeRate
                      exchangeRates={{ from: fromAccount.currencyCode, to: toAccount.currencyCode, rates }}
                    />
                  }
                />
              );
            })}
          </SwipeableContainer>
          <div className='exchange__keyboard'>
            <NumericKeyboard layout='tel' onPress={this.onInput} />
          </div>
        </div>
      </div>
    );
  }
}
