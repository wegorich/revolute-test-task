import React, { Component } from 'react';
import { DeviceFrame } from './components/DeviceFrame/DeviceFrame';
import { Exchange } from './pages/Exchange';
import { Provider, Subscribe } from 'unstated';
import { RatesContainer } from './models/rates';
import { AccountsContainer } from './models/accounts';

class App extends Component {
  render() {
    return (
      <div className="App">
        <DeviceFrame>
          <Provider>
            <Subscribe to={[RatesContainer, AccountsContainer]}>
              { (rates, accounts) => {
                return <Exchange
                  rates={rates.state.rates}
                  accounts={accounts.state.accounts}
                  ratesStartUpdates={rates.startUpdates}
                  ratesStopUpdates={rates.stopUpdates}
                  exchange={accounts.exchange}
                />
              }}
            </Subscribe>
          </Provider>
        </DeviceFrame>
      </div>
    );
  }
}

export default App;
